package idv.lcb.software.springSample.restfulService.get;

import java.util.HashMap;
import java.util.Map;

public class EmployeeCollection {

	public static final Map<String, Employee> employeeMap = new HashMap<>();

	static {
		employeeMap.put("0", new Employee("0", "Nobody"));
		employeeMap.put("1", new Employee("1", "Mike"));
		employeeMap.put("2", new Employee("2", "Mary"));
		employeeMap.put("3", new Employee("3", "Tom"));
	}
}
