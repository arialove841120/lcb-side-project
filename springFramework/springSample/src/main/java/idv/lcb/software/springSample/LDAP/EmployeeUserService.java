package idv.lcb.software.springSample.LDAP;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class EmployeeUserService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// 模擬DB
		Employee user = new Employee();
		user.setUsername("user");
		user.setPassword("$2a$10$RCHJluczkSO8ZIr44ob5Ket3RgyK2Rtt8eixqPc4/POLjlpomKl5S");
		user.setEnabled(true);

		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, this.getAuthorities("ROLE_USER"));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(String role) {
		return Collections.singletonList(new SimpleGrantedAuthority(role));
	}
}
