package idv.lcb.software.springSample.shcedule;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "scheduling.enabled", havingValue = "true") // 客製化，將 schedule 控制交給 property
@EnableScheduling
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		log.info("The time is now {} - reportCurrentTime1", dateFormat.format(new Date()));
	}

	/**
	 * second minute hour dayOfMonth month dayOfWeek [year] <br>
	 * second: 秒（0-59）<br>
	 * minute: 分鐘（0-59）<br>
	 * hour: 小時（0-23）<br>
	 * dayOfMonth: 月份中的日期（1-31）<br>
	 * month: 月份（1-12或者使用JAN-DEC的縮寫）<br>
	 * dayOfWeek: 星期中的日期（0-7或者使用SUN-SAT的縮寫，0和7代表周日）<br>
	 * [year]: 可選字段，年份（可選範圍）<br>
	 */
	@Scheduled(cron = "*/10 * * * * *")
	public void reportCurrentTime2() {
		log.info("The time is now {} - reportCurrentTime2", dateFormat.format(new Date()));
	}
}
