package idv.lcb.software.springSample.restfulService.get;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class RestfulGetController {

	/**
	 * URL = http://localhost:8080/api/v1/getEmployeeById?id=xxx
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/getEmployeeById")
	public Employee getEmployeeById1(@RequestParam(value = "id", defaultValue = "0") String id) {
		return EmployeeCollection.employeeMap.get(id);

	}

	/**
	 * URL = http://localhost:8080/api/v1/getEmployeeById/xxx
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/getEmployeeById/{id}")
	public Employee getEmployeeById3(@PathVariable("id") String id) {
		return EmployeeCollection.employeeMap.get(id);

	}
}
