package idv.lcb.software.springSample.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("api/v1/redis")
public class RedisController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisController.class);

	@Autowired
	private StringRedisTemplate template;

	/**
	 * 傳送 message 給 chat channel
	 * 
	 * @param message
	 * @return
	 */
	@PostMapping("/sendMessage")
	public String sendMessage(@RequestBody String message) {
		LOGGER.info("Sending message...");
		template.convertAndSend("chat", message);

		return "You Send A Message : " + message;
	}

	/**
	 * 將 employee 資訊存入 redis by key-balue
	 * 
	 * @param employee
	 * @return
	 */
	@PostMapping("/addCache")
	public String addCache(@RequestBody Employee employee) {
		LOGGER.info("Set Cache...");
		template.opsForValue()
				.set(employee.getId(), employee.getName());

		LOGGER.info("Get Cache...");
		return template	.opsForValue()
						.get(employee.getId());
	}
}
