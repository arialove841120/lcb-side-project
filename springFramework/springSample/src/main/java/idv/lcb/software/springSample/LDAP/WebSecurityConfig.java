package idv.lcb.software.springSample.LDAP;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebSecurityConfig {

	@Autowired
	private EmployeeUserService employeeUserService;

	/**
	 * 設定安全性規則
	 * 
	 * @param http
	 * @return
	 * @throws Exception
	 */
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

		http.authorizeHttpRequests(authorizeRequests -> authorizeRequests	.requestMatchers("/") // 設定遇到 http://localhost:8080/ 則檢核
																			.authenticated()
																			.anyRequest()
																			.permitAll())
			.formLogin(Customizer.withDefaults())
			.logout(Customizer.withDefaults())
			.csrf(csrf -> csrf.disable()); // 將 CSRF 取消(為了讓 POST 先過)

		return http.build();
	}

	/**
	 * 實作 UserDetailsService <br>
	 * 適用於使用者資料來自DB <br>
	 * 
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(employeeUserService);
	}

	/**
	 * 讀取ldif檔 <br>
	 * 
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configure2(AuthenticationManagerBuilder auth) throws Exception {
		auth.ldapAuthentication()
			.userDnPatterns("uid={0},ou=people")
			.groupSearchBase("ou=groups")
			.contextSource()
			.url("ldap://localhost:8389/dc=springframework,dc=org")
			.and()
			.passwordCompare()
			.passwordEncoder(new BCryptPasswordEncoder())
			.passwordAttribute("userPassword");
	}

	/**
	 * for configure 使用，需指定 PasswordEncoder，否則會拋 There is no PasswordEncoder mapped
	 * for the id "null"
	 * 
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * 測試 BCryptPasswordEncoder 加解密
	 */
	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		// 明文
		String rawPassword = "password";
		// 加密
		String encodedPassword = encoder.encode(rawPassword);
		System.out.println(encodedPassword);

		// 密文
		String encodedPasswordFromDatabase = "$2a$10$RCHJluczkSO8ZIr44ob5Ket3RgyK2Rtt8eixqPc4/POLjlpomKl5S";
		// 明文、密文比對
		boolean isPasswordMatch = encoder.matches(rawPassword, encodedPasswordFromDatabase);

		if (isPasswordMatch) {
			System.out.println("匹配");
		} else {
			System.out.println("不匹配");
		}

	}
}
