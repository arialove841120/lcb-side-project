package idv.lcb.software.provider;

import java.util.HashMap;
import java.util.Map;

public class EmployeeCollection {

	public static Map<String, String> employeeMap = new HashMap<>();

	static {
		employeeMap.put("1", "Mike");
		employeeMap.put("2", "Amy");
		employeeMap.put("3", "Chris");
	}
}