package idv.lcb.software.provider;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class ProviderController {

	@GetMapping("/getEmployeeMike")
	public Employee getEmployeeMike() {
		return new Employee("1", "Mike");
	}

	@GetMapping("/getEmployeeList")
	public List<Employee> getEmployeeList() {

		Map<String, String> employeeMap = EmployeeCollection.employeeMap;

		return employeeMap	.entrySet()
							.stream()
							.map(entry -> new Employee(entry.getKey(), entry.getValue()))
							.collect(Collectors.toList());
	}

	@GetMapping("/getEmployeeById")
	public Employee getEmployeeById(@RequestParam(value = "id", defaultValue = "0") String id) {
		Map<String, String> employeeMap = EmployeeCollection.employeeMap;

		return new Employee(id, employeeMap.get(id));
	}

	@GetMapping("/getEmployeeById2/{id}")
	public Employee getEmployeeById2(@PathVariable String id) {
		Map<String, String> employeeMap = EmployeeCollection.employeeMap;

		return new Employee(id, employeeMap.get(id));
	}
}
