package idv.lcb.software.consumer;

import java.util.ArrayList;
import java.util.List;

public class EmployeeList {

	private List<Employee> employeeList;

	public EmployeeList() {
		employeeList = new ArrayList<>();
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

}
