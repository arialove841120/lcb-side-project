package idv.lcb.software.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

	private static final String URL = "http://localhost:8080/provider/";

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 指定取得員工(方法一) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Object 物件
	 * 
	 * @return Employee 員工物件
	 */
	@GetMapping("/getEmployeeMike")
	public Employee getEmployeeMike() {
		return restTemplate.getForObject(URL + "getEmployeeMike", Employee.class);
	}

	/**
	 * 指定取得員工(方法二) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Record 物件
	 * 
	 * @return Employee2 員工物件
	 */
	@GetMapping("/getEmployee2Mike")
	public Employee2 getEmployee2Mike() {
		return restTemplate.getForObject(URL + "getEmployeeMike", Employee2.class);
	}

	/**
	 * 取得全部員工清單(方法一) <br>
	 * 使用 getForEntity 將 JSON 字串轉換成 Arrays
	 * 
	 * @return List<Employee> 員工清單
	 */
	@GetMapping("/getEmployeeList")
	public List<Employee> getEmployeeList() {
		ResponseEntity<Employee[]> response = restTemplate.getForEntity(URL + "getEmployeeList", Employee[].class);

		List<Employee> employeeList = new ArrayList<>();

		for (Employee employee : response.getBody()) {
			employeeList.add(employee);
		}

		return employeeList;
	}

	/**
	 * 取得全部員工清單(方法二) <br>
	 * 使用 exchange 將 JSON 字串轉換成 List
	 * 
	 * @return List<Employee> 員工清單
	 */
	@GetMapping("/getEmployeeList2")
	public List<Employee> getEmployeeList2() {

		ResponseEntity<List<Employee>> response = restTemplate.exchange(URL + "getEmployeeList", HttpMethod.GET, null, new ParameterizedTypeReference<List<Employee>>() {
		});

		return response.getBody();
	}

	/**
	 * 取得員工 By id (案例一/方法一) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Object
	 * 
	 * @param id 員工id
	 * @return Employee 員工物件
	 */
	@GetMapping("/getEmployeeById")
	public Employee getEmployeeById(@RequestParam(value = "id", defaultValue = "0") String id) {
		return restTemplate.getForObject(URL + "getEmployeeById?id=" + id, Employee.class);
	}

	/**
	 * 取得員工 By id (案例一/方法二) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Object <br>
	 * 使用 UriComponentsBuilder 組成 new URL
	 * 
	 * @param id 員工id
	 * @return Employee 員工物件
	 */
	@GetMapping("/getEmployeeById2")
	public Employee getEmployeeById2(@RequestParam(value = "id", defaultValue = "0") String id) {
		UriComponentsBuilder builder = UriComponentsBuilder	.fromHttpUrl(URL + "getEmployeeById")
															.queryParam("id", id);
		String newUrl = builder.toUriString();

		return restTemplate.getForObject(newUrl, Employee.class);
	}

	/**
	 * 取得員工 By id (案例二/方法一) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Object
	 * 
	 * @param id 員工id
	 * @return Employee 員工物件
	 */
	@GetMapping("/getEmployeeById3/{id}")
	public Employee getEmployeeById3(@PathVariable String id) {
		return restTemplate.getForObject(URL + "getEmployeeById2/{id}", Employee.class, id);
	}

	/**
	 * 取得員工 By id (案例二/方法二) <br>
	 * 使用 getForObject 將 JSON 字串轉換成 Object
	 * 
	 * @param id 員工id
	 * @return Employee 員工物件
	 */
	@GetMapping("/getEmployeeById4/{id}")
	public Employee getEmployeeById4(@PathVariable String id) {
		Map<String, String> uriVariables = new HashMap<>();
		uriVariables.put("id", id);

		return restTemplate.getForObject(URL + "getEmployeeById2/{id}", Employee.class, uriVariables);
	}
}
