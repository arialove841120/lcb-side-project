package idv.lcb.software.designPattern.behavioralPatterns.templateMethodPattern;

public class WarriorAttack implements AttackMethod {

	@Override
	public void pickUpWeapon() {
		System.out.println("Pick up a sword");
	}

	@Override
	public void moveForward() {
		System.out.println("Move slowly");
	}

	@Override
	public void hitEnemy() {
		System.out.println("Use sword to hit the enemy");
	}

	@Override
	public void putDownWeapon() {
		System.out.println("Put down the sword");
	}

}
