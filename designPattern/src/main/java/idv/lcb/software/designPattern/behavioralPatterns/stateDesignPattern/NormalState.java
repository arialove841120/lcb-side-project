package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class NormalState implements HeroState {

	@Override
	public void run() {
		System.out.println("You are running");
	}

	@Override
	public void useSkill() {
		System.out.println("Your are using skill");
	}

	@Override
	public void attack() {
		System.out.println("Your are attacking");
	}

}
