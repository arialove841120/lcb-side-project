package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

public enum HeroType {

	MAGICIAN, WARRIOR;
}
