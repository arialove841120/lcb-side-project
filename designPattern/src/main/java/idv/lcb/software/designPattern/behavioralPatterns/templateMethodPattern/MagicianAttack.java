package idv.lcb.software.designPattern.behavioralPatterns.templateMethodPattern;

public class MagicianAttack implements AttackMethod {

	@Override
	public void pickUpWeapon() {
		System.out.println("Pick up a stick");
	}

	@Override
	public void moveForward() {
		System.out.println("Move slowly");
	}

	@Override
	public void hitEnemy() {
		System.out.println("Use stick to hit the enemy");
	}

	@Override
	public void putDownWeapon() {
		System.out.println("Put down the stick");
	}

}
