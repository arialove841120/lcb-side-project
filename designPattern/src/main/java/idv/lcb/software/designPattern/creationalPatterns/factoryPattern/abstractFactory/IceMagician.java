package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class IceMagician implements Magician {

	@Override
	public void magicAttack() {
		System.out.println("Use ice ball to attack");
	}

}
