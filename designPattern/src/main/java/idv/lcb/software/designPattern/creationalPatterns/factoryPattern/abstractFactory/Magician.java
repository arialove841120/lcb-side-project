package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public interface Magician {

	void magicAttack();
}
