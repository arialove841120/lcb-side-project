package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

/**
 * High Hero 實作 HeroBuilder
 * 
 * @author chienpang
 *
 */
public class HighHeroBuilder extends HeroBuilder {

	@Override
	public void setHair() {
		hero.setHair("high");
	}

	@Override
	public void setBody() {
		hero.setBody("high");
	}
}
