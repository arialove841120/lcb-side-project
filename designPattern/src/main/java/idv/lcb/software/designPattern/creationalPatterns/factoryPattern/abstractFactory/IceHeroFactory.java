package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class IceHeroFactory implements HeroFactory {

	@Override
	public Magician makeMagician() {
		return new IceMagician();
	}

	@Override
	public Warrior makeWarrior() {
		return new IceWarrior();
	}

}
