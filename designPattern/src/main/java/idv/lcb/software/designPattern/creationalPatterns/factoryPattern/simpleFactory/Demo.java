package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

public class Demo {

	public static void main(String[] args) {
		HeroFactory heroFactory = new HeroFactory();
		Hero magician = heroFactory.makeHero(HeroType.MAGICIAN);
		magician.attack();

		Hero warrior = heroFactory.makeHero(HeroType.WARRIOR);
		warrior.attack();
	}
}
