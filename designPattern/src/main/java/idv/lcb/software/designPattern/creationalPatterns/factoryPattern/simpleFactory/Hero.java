package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

public interface Hero {

	void attack();
}
