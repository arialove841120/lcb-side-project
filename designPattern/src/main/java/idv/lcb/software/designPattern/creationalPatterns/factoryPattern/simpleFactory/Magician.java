package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

public class Magician implements Hero {

	@Override
	public void attack() {
		System.out.println("Use magic to attack");
	}

}
