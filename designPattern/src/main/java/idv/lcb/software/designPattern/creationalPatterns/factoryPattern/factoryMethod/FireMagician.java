package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public class FireMagician implements Magician {

	@Override
	public void attack() {
		System.out.println("Use staff to attack");
	}

	@Override
	public void magicAttack() {
		System.out.println("Use fire ball to attack");
	}

}
