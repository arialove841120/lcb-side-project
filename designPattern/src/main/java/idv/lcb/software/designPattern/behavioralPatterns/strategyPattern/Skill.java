package idv.lcb.software.designPattern.behavioralPatterns.strategyPattern;

public interface Skill {

	void doAction();
}
