package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class Hero {

	HeroState heroState;

	public void attack() {
		heroState.attack();
	}

	public void run() {
		heroState.run();
	}

	public void useSkill() {
		heroState.useSkill();
	}

	public void setState(HeroState state) {
		this.heroState = state;
	}
}
