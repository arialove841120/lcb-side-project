package idv.lcb.software.designPattern.creationalPatterns.singleton;

/**
 * 懶漢(懶加載) <br>
 * 關鍵字: volatile、synchronized
 * 
 * @author chienpang
 *
 */
public class LazySingleton {

	private static volatile LazySingleton instance;

	private LazySingleton() {
	}

	public static LazySingleton getInstance() {
		if (instance == null) {
			synchronized (LazySingleton.class) {
				if (instance == null) {
					instance = new LazySingleton();
				}
			}
		}
		return instance;
	}
}
