package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class PetrochemicalState implements HeroState {

	@Override
	public void run() {
		System.out.println("Your are a stone so you can't do anything");
	}

	@Override
	public void useSkill() {
		System.out.println("Your are a stone so you can't do anything");
	}

	@Override
	public void attack() {
		System.out.println("Your are a stone so you can't do anything");
	}

}
