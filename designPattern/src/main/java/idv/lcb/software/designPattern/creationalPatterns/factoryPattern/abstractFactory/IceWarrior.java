package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class IceWarrior implements Warrior {

	@Override
	public void swordAttack() {
		System.out.println("Use ice sword to attack");
	}

}
