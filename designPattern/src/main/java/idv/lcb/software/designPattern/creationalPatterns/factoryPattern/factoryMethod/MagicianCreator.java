package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public class MagicianCreator implements HeroCreator<MagicType, Magician> {

	@Override
	public Magician createHero(MagicType type) {

		if (type.equals(MagicType.ICE)) {
			return new IceMagician();
		} else if (type.equals(MagicType.FIRE)) {
			return new FireMagician();
		} else {
			return null;
		}
	}

}
