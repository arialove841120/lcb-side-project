package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class Demo {

	public static void main(String[] args) {
		Hero hero = new Hero();
		HeroActionService heroActionService = new HeroActionService();
		System.out.println("Normal state");
		hero.setState(new NormalState());
		heroActionService.doAllAction(hero);
		hero.setState(new CursedState());
		heroActionService.doAllAction(hero);
		hero.setState(new PetrochemicalState());
		heroActionService.doAllAction(hero);
	}
}
