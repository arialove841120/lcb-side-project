package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public interface Hero {

	void attack();
}
