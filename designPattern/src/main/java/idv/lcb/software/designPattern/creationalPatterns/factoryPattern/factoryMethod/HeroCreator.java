package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public interface HeroCreator<TYPE, HERO extends Hero> {

	HERO createHero(TYPE type);
}
