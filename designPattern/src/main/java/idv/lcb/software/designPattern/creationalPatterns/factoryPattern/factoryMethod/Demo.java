package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public class Demo {

	public static void main(String[] args) {
		MagicianCreator creator = new MagicianCreator();

		Magician iceMagican = creator.createHero(MagicType.ICE);

		iceMagican.attack();
		iceMagican.magicAttack();

		Magician fireMagician = creator.createHero(MagicType.FIRE);

		fireMagician.attack();
		fireMagician.magicAttack();
	}
}
