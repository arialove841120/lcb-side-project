package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public interface Magician extends Hero {

	void magicAttack();
}
