package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

public class Warrior implements Hero {

	@Override
	public void attack() {
		System.out.println("Use sword to attack");
	}

}
