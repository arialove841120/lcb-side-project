package idv.lcb.software.designPattern.behavioralPatterns.strategyPattern;

public class IceSkill implements Skill {

	@Override
	public void doAction() {
		System.out.println("Use ice to attack");
	}

}
