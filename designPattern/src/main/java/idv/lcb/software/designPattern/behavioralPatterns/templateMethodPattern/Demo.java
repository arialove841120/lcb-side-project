package idv.lcb.software.designPattern.behavioralPatterns.templateMethodPattern;

public class Demo {

	public static void main(String[] args) {
		MagicianAttack magicianAttack = new MagicianAttack();
		WarriorAttack warriorAttack = new WarriorAttack();
		Hero hero = new Hero();
		hero.attack(magicianAttack);
		hero.attack(warriorAttack);
	}
}
