package idv.lcb.software.designPattern.creationalPatterns.prototypePattern;

public class Sword implements Cloneable {

	/** 力量 STR */
	private int strength;
	/** 敏捷 AGI */
	private int agile;
	/** 智力 INT */
	private int intelligence;
	/** 幸運 LCK */
	private int lucky;

	public Sword() {
		this.strength = 10;
		this.agile = 2;
		this.intelligence = 0;
		this.lucky = 0;
	}

	public int getStrength() {
		return strength;
	}

	public int getAgile() {
		return agile;
	}

	public int getIntelligence() {
		return intelligence;
	}

	public int getLucky() {
		return lucky;
	}

	@Override
	public String toString() {
		return "STR: " + this.strength + " AGI: " + this.agile + " INT: " + this.intelligence + " LCK: " + this.lucky;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
