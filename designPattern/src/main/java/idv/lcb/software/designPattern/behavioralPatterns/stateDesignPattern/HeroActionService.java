package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class HeroActionService {
	public void doAllAction(Hero hero) {
		hero.attack();
		hero.run();
		hero.useSkill();
	}
}
