package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

/**
 * 利用 Builder 創建 Hero
 * 
 * @author chienpang
 *
 */
public class Camp {

	private HeroBuilder heroBuilder;

	public void setHeroBuilder(HeroBuilder heroBuilder) {
		this.heroBuilder = heroBuilder;
	}

	public Hero getHero() {
		return heroBuilder.getHero();
	}

	public void constructHero() {
		heroBuilder.createHero();
		heroBuilder.setBody();
		heroBuilder.setHair();
	}
}
