package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.simpleFactory;

/**
 * simple factory
 * 
 * @author chienpang
 *
 */
public class HeroFactory {

	public Hero makeHero(HeroType heroType) {
		if (heroType.equals(HeroType.MAGICIAN)) {
			return new Magician();
		} else if (heroType.equals(HeroType.WARRIOR)) {
			return new Warrior();
		} else {
			return null;
		}
	}
}
