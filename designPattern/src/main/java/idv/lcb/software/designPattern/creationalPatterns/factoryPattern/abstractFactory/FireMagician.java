package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class FireMagician implements Magician {

	@Override
	public void magicAttack() {
		System.out.println("Use fire ball to attack");
	}

}
