package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public class CursedState implements HeroState {

	@Override
	public void run() {
		System.out.println("You are running");
	}

	@Override
	public void useSkill() {
		System.out.println("Your are cursed so you can't use skill");
	}

	@Override
	public void attack() {
		System.out.println("Your are attacking");
	}

}
