package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public interface HeroFactory {
	Magician makeMagician();

	Warrior makeWarrior();
}
