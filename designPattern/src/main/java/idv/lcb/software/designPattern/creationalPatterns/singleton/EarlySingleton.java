package idv.lcb.software.designPattern.creationalPatterns.singleton;

/**
 * 餓漢(預加載) <br>
 * 關鍵字: final
 * 
 * @author chienpang
 *
 */
public class EarlySingleton {
	private EarlySingleton() {
	}

	private static class EarlySingletonHolder {
		private static final EarlySingleton INSTANCE = new EarlySingleton();
	}

	public static EarlySingleton getInstance() {
		return EarlySingletonHolder.INSTANCE;
	}
}
