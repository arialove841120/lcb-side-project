package idv.lcb.software.designPattern.behavioralPatterns.templateMethodPattern;

public interface AttackMethod {

	default void attack() {
		pickUpWeapon();
		moveForward();
		hitEnemy();
		putDownWeapon();
	};

	void pickUpWeapon();

	void moveForward();

	void hitEnemy();

	void putDownWeapon();
}
