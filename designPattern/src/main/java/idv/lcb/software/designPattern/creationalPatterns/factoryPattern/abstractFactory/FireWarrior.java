package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class FireWarrior implements Warrior {

	@Override
	public void swordAttack() {
		System.out.println("Use fire sword to attack");
	}

}
