package idv.lcb.software.designPattern.creationalPatterns.prototypePattern;

/**
 * 創建模式:原型模式-prototype pattern
 * 
 * @author chienpang
 */
public class Demo {

	public static void main(String[] args) {
		try {
			Sword sword = new Sword();
			// 複製
			Sword copySword = (Sword) sword.clone();

			System.out.println(sword);
			System.out.println(copySword);
			System.out.println(sword.equals(copySword));
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

	}

}
