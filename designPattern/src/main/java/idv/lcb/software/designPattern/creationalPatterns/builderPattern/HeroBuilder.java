package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

/**
 * Hero 創建器(抽象)
 * 
 * @author chienpang
 *
 */
public abstract class HeroBuilder {

	protected Hero hero;

	public void createHero() {
		this.hero = new Hero();
	}

	public Hero getHero() {
		return hero;
	}

	public abstract void setHair();

	public abstract void setBody();

}
