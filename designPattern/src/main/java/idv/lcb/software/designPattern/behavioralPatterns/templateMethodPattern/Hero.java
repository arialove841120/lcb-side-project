package idv.lcb.software.designPattern.behavioralPatterns.templateMethodPattern;

public class Hero {

	public void attack(AttackMethod attackMethod) {
		attackMethod.attack();
	}
}
