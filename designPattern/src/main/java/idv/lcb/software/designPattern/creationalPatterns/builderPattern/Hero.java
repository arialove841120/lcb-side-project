package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

/**
 * Hero 類別，包含基本數值
 * 
 * @author chienpang
 *
 */
public class Hero {

	String body;

	String hair;

	public void setBody(String body) {
		this.body = body;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	@Override
	public String toString() {
		return "Hero [body=" + body + ", hair=" + hair + "]";
	}

}
