package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

/**
 * Normal Hero 實作 HeroBuilder
 * 
 * @author chienpang
 *
 */
public class NormalHeroBuilder extends HeroBuilder {

	@Override
	public void setHair() {
		hero.setHair("normal");
	}

	@Override
	public void setBody() {
		hero.setBody("normal");
	}
}
