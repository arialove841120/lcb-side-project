package idv.lcb.software.designPattern.behavioralPatterns.strategyPattern;

public class Demo {

	public static void main(String[] args) {
		SkillService service = new SkillService();

		service.doAction("FireAttack");
		service.doAction("IceAttack");
	}
}
