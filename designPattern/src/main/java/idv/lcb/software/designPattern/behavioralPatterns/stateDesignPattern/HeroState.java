package idv.lcb.software.designPattern.behavioralPatterns.stateDesignPattern;

public interface HeroState {

	void run();

	void useSkill();

	void attack();
}
