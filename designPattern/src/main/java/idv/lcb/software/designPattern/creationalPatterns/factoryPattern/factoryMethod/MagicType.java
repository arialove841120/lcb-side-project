package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public enum MagicType {

	ICE, FIRE;
}
