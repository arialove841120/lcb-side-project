package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.factoryMethod;

public class IceMagician implements Magician {

	@Override
	public void attack() {
		System.out.println("Use staff to attack");
	}

	@Override
	public void magicAttack() {
		System.out.println("Use ice ball to attack");
	}

}
