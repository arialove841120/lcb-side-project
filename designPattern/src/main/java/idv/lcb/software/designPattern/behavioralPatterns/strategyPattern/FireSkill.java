package idv.lcb.software.designPattern.behavioralPatterns.strategyPattern;

public class FireSkill implements Skill {

	@Override
	public void doAction() {
		System.out.println("Use fire to attack");
	}

}
