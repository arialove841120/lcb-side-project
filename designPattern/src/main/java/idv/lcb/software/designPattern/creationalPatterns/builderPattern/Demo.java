package idv.lcb.software.designPattern.creationalPatterns.builderPattern;

import java.util.Scanner;

/**
 * 創建模式:建構者模式-builder pattern
 * 
 * @author chienpang
 *
 */
public class Demo {

	public static void main(String[] args) {
		Camp heroCamp = new Camp();

		HeroBuilder heroBuilder = null;

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);

		String type = scanner.next();

		if (type.equals("Normal")) {
			heroBuilder = new NormalHeroBuilder();
		} else if (type.equals("High")) {
			heroBuilder = new HighHeroBuilder();
		}

		if (heroBuilder != null) {
			heroCamp.setHeroBuilder(heroBuilder);
			heroCamp.constructHero();
			Hero hero = heroCamp.getHero();
			System.out.println(hero);
		}
	}
}
