package idv.lcb.software.designPattern.creationalPatterns.singleton;

/**
 * 創建模式:單例模式-singleton pattern <br>
 * 使用時機:log、configuration、cache <br>
 * 分成: 懶漢、餓漢
 * 
 * @author chienpang
 *
 */
public class Demo {

	public static void main(String[] args) {
		System.out.println(EarlySingleton.getInstance());
		System.out.println(LazySingleton.getInstance());
	}
}
