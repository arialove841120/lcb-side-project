package idv.lcb.software.designPattern.creationalPatterns.factoryPattern.abstractFactory;

public class FireHeroFactory implements HeroFactory {

	@Override
	public Magician makeMagician() {
		return new FireMagician();
	}

	@Override
	public Warrior makeWarrior() {
		return new FireWarrior();
	}

}
