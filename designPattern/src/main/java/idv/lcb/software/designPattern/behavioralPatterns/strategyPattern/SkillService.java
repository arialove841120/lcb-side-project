package idv.lcb.software.designPattern.behavioralPatterns.strategyPattern;

import java.util.HashMap;
import java.util.Map;

public class SkillService {

	private Map<String, Skill> skillMap = new HashMap<>();

	public SkillService() {
		skillMap.put("FireAttack", new FireSkill());
		skillMap.put("IceAttack", new IceSkill());
	}

	public void doAction(String skillType) {
		Skill skill = skillMap.get(skillType);
		skill.doAction();
	}
}
