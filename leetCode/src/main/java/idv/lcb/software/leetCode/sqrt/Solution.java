package idv.lcb.software.leetCode.sqrt;

public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();
		int x = 0;
		System.out.println(solution.mySqrt(x));
	}

	public int mySqrt(int x) {

		if (x == 1) {
			return 1;
		}

		int max = x;

		int min = 0;

		do {
			long target = (long) ((max + min) / 2) * (long) ((max + min) / 2);

			if (x > target) {
				min = (max + min) / 2;
			} else if (x < target) {
				max = (max + min) / 2;
			} else {
				return (max + min) / 2;
			}
		} while (max - min > 1);

		return min;
	}
}
