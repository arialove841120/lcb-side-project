package idv.lcb.software.leetCode.palindromeNumber;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		int x = 121;

		System.out.println(solution.isPalindrome(x));
	}

	public boolean isPalindrome(int x) {

		if (x < 0) {
			return false;
		}

		String target = String.valueOf(x);

		char[] targetArr = target.toCharArray();

		int length = targetArr.length;

		for (int i = 0; i < length; i++) {
			if (Character.compare(targetArr[i], targetArr[length - i - 1]) != 0) {
				return false;
			}
		}

		return true;
	}
}
