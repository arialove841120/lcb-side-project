package idv.lcb.software.leetCode.removeDuplicatesFromSortedArray;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		int[] nums = { 1, 1, 2 };

		System.out.println(solution.removeDuplicates(nums));
	}

	public int removeDuplicates(int[] nums) {
		// default first element
		int now = nums[0];
		int nowIdx = 0;

		// second .. last element for loop
		for (int i = 1; i < nums.length; i++) {
			// 當走訪的元素不等於當前的元素
			if (nums[i] != now) {
				nums[nowIdx + 1] = nums[i];
				now = nums[i];
				nowIdx++;
			}
		}

		return nowIdx + 1;

	}
}
