package idv.lcb.software.leetCode.removeElement;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		int[] nums = { 2 };
		int val = 3;

		System.out.println(solution.removeElement(nums, val));
	}

	public int removeElement(int[] nums, int val) {

		int nowIdx = 0;

		for (int i = 0; i < nums.length; i++) {

			if (nums[i] == val) {
				nowIdx = i;
				// 將非移除的數字與移除目標交換
				for (int j = i + 1; j < nums.length; j++) {
					if (nums[j] != val) {
						nums[nowIdx] = nums[j];
						nums[j] = val;
						break;
					}
				}
			}

		}

		// 算出未移除長度
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == val) {
				return i;
			}
		}

		return nums.length;
	}
}
