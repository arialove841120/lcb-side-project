package idv.lcb.software.leetCode.validParentheses;

import java.util.Stack;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		String s = "]";

		System.out.println(solution.isValid(s));
	}

	public boolean isValid(String s) {

		char[] charArr = s.toCharArray();

		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < charArr.length; i++) {
			// 左括弧儲存
			if (charArr[i] == '(' || charArr[i] == '[' || charArr[i] == '{') {
				stack.push(charArr[i]);
			}
			// 右刮弧排除
			if (charArr[i] == ')' || charArr[i] == ']' || charArr[i] == '}') {

				if (stack.size() == 0) {
					return false;
				}

				char temp = stack.pop();

				switch (charArr[i]) {
				case ')':
					if (temp != '(') {
						return false;
					}
					break;
				case ']':
					if (temp != '[') {
						return false;
					}
					break;
				case '}':
					if (temp != '{') {
						return false;
					}
					break;

				default:
					break;
				}
			}
		}

		return stack.size() == 0 ? true : false;
	}
}
