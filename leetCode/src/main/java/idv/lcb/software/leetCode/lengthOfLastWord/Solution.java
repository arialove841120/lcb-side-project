package idv.lcb.software.leetCode.lengthOfLastWord;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();
		String s = "Hello World";
		System.out.println(solution.lengthOfLastWord(s));

	}

	public int lengthOfLastWord(String s) {

		String[] arr = s.split(" ");

		for (int i = arr.length; i > 0; i--) {

			if (arr[i - 1].length() > 0) {
				return arr[i - 1].length();
			}
		}

		return 0;
	}
}
