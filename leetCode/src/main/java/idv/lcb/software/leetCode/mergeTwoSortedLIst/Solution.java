package idv.lcb.software.leetCode.mergeTwoSortedLIst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {

	}

	public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
		// 將 list1 list2 湊成一個 list
		List<Integer> list = new ArrayList<>();

		ListNode temp1 = list1;
		ListNode temp2 = list2;

		while (temp1 != null) {
			list.add(temp1.val);
			temp1 = temp1.next;
		}

		while (temp2 != null) {
			list.add(temp2.val);
			temp2 = temp2.next;
		}

		if (list.isEmpty()) {
			return null;
		}

		// 依大小排序
		Collections.sort(list);

		// 從小至大串接
		ListNode ans = null;
		ListNode now = null;

		for (Integer val : list) {
			ListNode node = new ListNode(val);

			if (ans == null) {
				ans = node;
			} else {
				now.next = node;
			}

			now = node;
		}

		return ans;
	}

	// Definition for singly-linked list.
	public class ListNode {
		int val;

		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

}
