package idv.lcb.software.leetCode.plusOne;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();
		int[] digits = { 9 };

		int[] ans = solution.plusOne(digits);

		for (int i : ans) {
			System.out.print(i);
		}
	}

	public int[] plusOne(int[] digits) {

		int length = digits.length;

		for (int i = 0; i < length; i++) {
			if (digits[length - i - 1] + 1 == 10) {
				digits[length - i - 1] = 0;
			} else {
				digits[length - i - 1] = digits[length - i - 1] + 1;
				return digits;
			}
		}

		int[] ans = new int[digits.length + 1];

		if (digits[0] == 0) {
			ans[0] = 1;
		}

		return ans;

	}
}
