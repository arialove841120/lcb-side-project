package idv.lcb.software.leetCode.addBinary;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();
		String a = "1";
		String b = "111";
		System.out.println(solution.addBinary(a, b));
	}

	public String addBinary(String a, String b) {

		/**
		 * 將 a、b 字串 reverse
		 */
		char[] aChar = a.toCharArray();
		StringBuilder aBuilder = new StringBuilder();

		for (int i = 0; i < aChar.length; i++) {
			aBuilder.append(aChar[aChar.length - i - 1]);
		}

		char[] bChar = b.toCharArray();
		StringBuilder bBuilder = new StringBuilder();

		for (int i = 0; i < bChar.length; i++) {
			bBuilder.append(bChar[bChar.length - i - 1]);
		}

		// 取用最長字串當作迴圈目標
		char[] target = aChar.length > bChar.length ? aChar : bChar;

		StringBuilder targetBuilder = new StringBuilder();

		char next = '0';

		for (int i = 0; i < target.length; i++) {
			// 兩字串長度相差部分
			if (aChar.length <= i || bChar.length <= i) {
				StringBuilder builder = aChar.length <= i ? bBuilder : aBuilder;
				if (builder.charAt(i) == '1' && next == '1') {
					targetBuilder.append("0");
				} else if (next == '1') {
					targetBuilder.append("1");
					next = '0';
				} else {
					targetBuilder.append(builder.charAt(i));
				}
			} else { // 兩字串長度相等部分
				// 有進位
				if (next == '1') {
					if (aBuilder.charAt(i) == '1' && bBuilder.charAt(i) == '1') {
						targetBuilder.append("1");
						next = '1';
					} else if (aBuilder.charAt(i) == '1' || bBuilder.charAt(i) == '1') {
						targetBuilder.append("0");
						next = '1';
					} else {
						targetBuilder.append("1");
						next = '0';
					}

				} else { // 無進位
					if (aBuilder.charAt(i) == '1' && bBuilder.charAt(i) == '1') {
						targetBuilder.append("0");
						next = '1';
					} else if (aBuilder.charAt(i) == '1' || bBuilder.charAt(i) == '1') {
						targetBuilder.append("1");
					} else {
						targetBuilder.append("0");
					}
				}
			}
		}

		StringBuilder ansBuilder = new StringBuilder();
		// 最後有進位則補上
		if (next == '1') {
			targetBuilder.append('1');
		}

		// 將最後答案 reverse
		for (int i = 0; i < targetBuilder.length(); i++) {
			ansBuilder.append(targetBuilder.charAt(targetBuilder.length() - i - 1));
		}

		return ansBuilder.toString();
	}

	/**
	 * best answer
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public String addBinary2(String a, String b) {
		StringBuilder result = new StringBuilder();

		int carry = 0;
		int i = a.length() - 1;
		int j = b.length() - 1;

		while (i >= 0 || j >= 0 || carry > 0) {
			int digitA = (i >= 0) ? a.charAt(i--) - '0' : 0;
			int digitB = (j >= 0) ? b.charAt(j--) - '0' : 0;

			int sum = digitA + digitB + carry;
			result.insert(0, sum % 2);
			carry = sum / 2;
		}

		return result.toString();
	}
}
