package idv.lcb.software.leetCode.twoSum;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 1. Two Sum
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		int[] test = { 3, 2, 4 };

		Solution solution = new Solution();
		int[] ans = solution.twoSum(test, 6);

		System.out.println(ans[0] + "," + ans[1]);

		int[] ans2 = solution.twoSum2(test, 6);

		System.out.println(ans2[0] + "," + ans2[1]);
	}

	public int[] twoSum(int[] nums, int target) {

		int length = nums.length;

		for (int i = 0; i < length - 1; i++) {
			for (int j = i + 1; j < length; j++) {
				if (target == nums[i] + nums[j]) {
					return new int[] { i, j };
				}
			}
		}

		return null;
	}

	public int[] twoSum2(int[] nums, int target) {

		List<Integer> numsList = Arrays	.stream(nums)
										.boxed()
										.collect(Collectors.toList());

		Map<Integer, Integer> numsMap = IntStream	.range(0, numsList.size())
													.boxed()
													.collect(Collectors.toMap(idx -> idx, numsList::get));

		for (int i = 0; i < nums.length; i++) {
			for (Map.Entry<Integer, Integer> entry : numsMap.entrySet()) {
				if (i != entry.getKey() && target == nums[i] + entry.getValue()) {
					return i > entry.getKey() ? new int[] { entry.getKey(), i } : new int[] { i, entry.getKey() };
				}
			}
		}

		return null;
	}
}
