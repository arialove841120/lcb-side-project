package idv.lcb.software.leetCode.longestCommonPrefix;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		String[] strs = { "a", "a", "aabc", "aa", "acc" };

		System.out.println(solution.longestCommonPrefix(strs));
	}

	public String longestCommonPrefix(String[] strs) {

		String minWord = strs[0];

		String ans = "";

		// 取得字串陣列最小長度字串
		for (int i = 0; i < strs.length; i++) {

			if (minWord.length() > strs[i].length()) {
				minWord = strs[i];
			}
		}

		// 最小長度字串為空
		if (minWord.equals("")) {
			return "";
		}

		// 使用最小長度字串比對
		for (int i = 0; i < minWord.length(); i++) {
			for (int j = 0; j < strs.length; j++) {
				if (minWord.charAt(i) != strs[j].charAt(i)) {
					return ans;
				}
			}
			ans = ans + minWord.charAt(i);
		}

		return ans;
	}
}
