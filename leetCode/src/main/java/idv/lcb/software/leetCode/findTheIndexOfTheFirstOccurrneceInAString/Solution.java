package idv.lcb.software.leetCode.findTheIndexOfTheFirstOccurrneceInAString;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {

		Solution solution = new Solution();

		String haystack = "sadbutsad";

		String needle = "sad";

		System.out.println(solution.strStr(haystack, needle));
		System.out.println(solution.strStr2(haystack, needle));

	}

	public int strStr(String haystack, String needle) {

		return haystack.indexOf(needle);

	}

	public int strStr2(String haystack, String needle) {

		if (haystack.length() < needle.length()) {
			return -1;
		}

		int haystackLength = haystack.length();

		int needleLength = needle.length();

		char[] haystackArr = haystack.toCharArray();

		char[] needleArr = needle.toCharArray();

		int ans = -1;

		for (int i = 0; i < haystackLength; i++) {

			if (haystackArr[i] == needleArr[0] && haystackLength - i >= needleLength) {

				ans = i;

				for (int j = 0; j < needleLength; j++) {
					if (haystackArr[i + j] != needleArr[j]) {
						ans = -1;
						break;
					}
				}

				if (ans != -1) {
					return ans;
				}
			}
		}

		return -1;

	}
}
