package idv.lcb.software.leetCode.romanToInt;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author chienpang
 *
 */
public class Solution {

	public static void main(String[] args) {
		Solution solution = new Solution();

		String s = "MCMXCVI";

		System.out.println(solution.romanToInt(s));
		System.out.println(solution.romanToInt2(s));
	}

	public int romanToInt(String s) {

		int total = 0;

		int nowLength = s.length();

		String target = s;

		String[] otherArr = { "IV", "IX", "XL", "XC", "CD", "CM" };

		for (String other : otherArr) {
			String[] tempArr = target.split(other);

			if (tempArr.length == 2) {
				target = tempArr[0] + tempArr[1];
			} else if (tempArr.length == 1) {
				target = tempArr[0];
			} else {
				target = "";
			}

			if (nowLength != target.length()) {
				nowLength = target.length();

				switch (other) {
				case "IV":
					total += 4;
					break;
				case "IX":
					total += 9;
					break;
				case "XL":
					total += 40;
					break;
				case "XC":
					total += 90;
					break;
				case "CD":
					total += 400;
					break;
				case "CM":
					total += 900;
					break;
				default:
					break;
				}
			} else {
				target = tempArr[0];
			}
		}

		char[] targetArr = target.toCharArray();

		if (targetArr.length == 0) {
			return total;
		} else {
			for (int i = 0; i < targetArr.length; i++) {
				switch (targetArr[i]) {
				case 'I':
					total += 1;
					break;
				case 'V':
					total += 5;
					break;
				case 'X':
					total += 10;
					break;
				case 'L':
					total += 50;
					break;
				case 'C':
					total += 100;
					break;
				case 'D':
					total += 500;
					break;
				case 'M':
					total += 1000;
					break;
				default:
					break;
				}
			}
		}

		return total;
	}

	public int romanToInt2(String s) {

		Map<Character, Integer> romanMap = new HashMap<>();
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);
		romanMap.put('C', 100);
		romanMap.put('D', 500);
		romanMap.put('M', 1000);

		int total = 0;
		char[] targetArr = s.toCharArray();

		for (int i = 0; i < targetArr.length; i++) {
			if (i < targetArr.length - 1 && romanMap.get(targetArr[i]) < romanMap.get(targetArr[i + 1])) {
				total -= romanMap.get(targetArr[i]);
			} else {
				total += romanMap.get(targetArr[i]);
			}
		}

		return total;
	}
}
